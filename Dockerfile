FROM debian:buster-slim

ARG S6_RELEASE=v2.2.0.0
ARG SOGO_RELEASE=5
LABEL maintainer="dustin"
EXPOSE 80/tcp

ADD https://github.com/just-containers/s6-overlay/releases/download/$S6_RELEASE/s6-overlay-amd64.tar.gz /tmp/s6-overlay.tar.gz
RUN tar xvfz /tmp/s6-overlay.tar.gz -C /

RUN apt-get update && apt-get install -y \
    apache2 \
    apt-transport-https \
    ca-certificates \
    gpg \
  && a2enmod proxy_http \
  && gpg --keyserver hkp://keys.gnupg.net --recv-key 0x810273C4 \
  && gpg --armor --export 0x810273C4 | apt-key add - \
  && echo "SOGO_RELEASE=$SOGO_RELEASE" \
  && echo "deb https://packages.inverse.ca/SOGo/nightly/${SOGO_RELEASE}/debian/ buster buster" > /etc/apt/sources.list.d/SOGo.list \
  && apt update \
  && mkdir -p /usr/share/doc/sogo && touch /usr/share/doc/sogo/empty.sh \
  && apt install -y \
     sogo \
     sope4.9-gdl1-postgresql \
     sope4.9-gdl1-mysql

ENTRYPOINT ["/init"]

COPY SOGo.apache.conf /etc/apache2/conf-enabled/SOGo.conf
COPY services.d /etc/services.d
